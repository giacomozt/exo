#ifndef FFT_HPP
#define FFT_HPP

#include <unordered_map>
#include <complex>
#include "AudioDataBuffer.hpp"
#include <fstream> //

#define PI 3.14159265359
#define BASS_BIN_SIZE 5
#define BASS_HISTORY_SIZE 256
#define BASS_Z_MULT 1.35

// class for calculated Cooley-Tukey fast fourier transform of live audio data
class FFT {
protected:
    // nested class to store a circular buffer of data
    template <typename T>
    class CircularArray {
    protected:
        size_t mSize;
        T* mArray;
        
    public:
        CircularArray(size_t size) {
            mSize = size;
            mArray = new T[mSize];
            for (size_t h = 0; h<mSize; ++h) mArray[h] = static_cast<T>(0);
        }
        
        ~CircularArray() {
            delete[] mArray;
        }
        
        T& operator[] (size_t index);
        
        void push(T value);
        
        size_t debug() {
            return mSize;
        }
    };
    
    unsigned int mNumSamps;
    unsigned int mDegree;
    unsigned int* mRadixMap;
    float mFreqRes;
    float* mOut;
    AudioDataBuffer* mBuffer;
    std::complex<float>** flip;
    std::complex<float>** flop;
    unsigned int mLongAvgCount;
    unsigned int mShortAvgCount;
    bool mBassBeat;
    CircularArray<float>* mBassHistory;
    std::ofstream bass; //**
    std::ofstream bassAvg;
    std::ofstream bassBound;
    
    template <typename T>
    static void swap(T** flip, T** flop);
    static unsigned int bitReverse(unsigned int number, unsigned int numBits);
    static std::complex<float> invRootOfUnity(unsigned int k, unsigned int N);
    
    std::complex<float>* analyzeRecurse(std::complex<float>** read, std::complex<float>** write, unsigned int depth);
    
public:
    FFT(AudioDataBuffer* buffer, size_t numSamps, float sampTime) {
        mBuffer = buffer;
        mDegree = ceil(log(numSamps)/log(2.0))+1;
        mNumSamps = 1<<mDegree;
        mFreqRes = 1.0/(static_cast<unsigned int>(mNumSamps)*sampTime);
        mOut = new float[mNumSamps/2];
        mRadixMap = new unsigned int[mNumSamps];
        for (unsigned int h = 0; h<mNumSamps/2; ++h) mRadixMap[h] = bitReverse(h, mDegree);
        for (unsigned int h = mNumSamps/2; h<mNumSamps; ++h) mRadixMap[h] = mRadixMap[h-mNumSamps/2]+1;
        flip = new std::complex<float>*;
        flop = new std::complex<float>*;
        *flip = new std::complex<float>[mNumSamps];
        *flop = new std::complex<float>[mNumSamps];
        
        mBassHistory = new CircularArray<float>(BASS_HISTORY_SIZE);
    }
    
    ~FFT() {
        delete[] mOut;
        delete[] mRadixMap;
        delete[] *flip;
        delete[] *flop;
        delete flip;
        delete flop;
        delete mBassHistory;
    }
    
    bool bassBeat() {
        return mBassBeat;
    }
    
    size_t size() {
        return mNumSamps/2;
    }
    
    float amplitude(size_t bin) { // TODO: possible speed up by running calculation as requested?
        return mOut[bin];
    }
    
    float resolution() {
        return mFreqRes;
    }
    
    void analyze(float pos, bool calcBass);
};

#endif