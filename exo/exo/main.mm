//
//  main.m
//  exo
//
//  Created by Giacomo Zavolta Taylor on 11/18/16.
//  Copyright © 2016 Giacomo Zavolta Taylor. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include "AppDelegate.h"

int main(int argc, const char** argv) {
    [NSApplication sharedApplication];
    
    AppDelegate *appDelegate = [[AppDelegate alloc] init];
    [NSApp setDelegate:appDelegate];
    [NSApp run];
    
    return 0;
}
