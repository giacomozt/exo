#ifndef WAVEFORMVIEW_H
#define WAVEFORMVIEW_H
#import <Cocoa/Cocoa.h>
#include "AudioDataBuffer.hpp"
#include "DFT.hpp" //
#include "FFT.hpp"

@interface WaveFormView : NSView {
    Float64 readPos;
    AudioDataBuffer* buffer;
    FFT* fft;
    int controller;
}
- (id)initWithFrame:(NSRect)frameRect buffer:(AudioDataBuffer*)_buffer;
- (void)drawRect:(NSRect)dirtyRect;
- (void)draw:(Float64)time;
- (void)initController;
@end

#endif