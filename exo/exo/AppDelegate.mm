#import "AppDelegate.h"

@implementation AppDelegate

// initialize and allocate all necessary memory
- (void) applicationDidFinishLaunching:(NSNotification *)notification {
    devices = new std::vector<AudioDevice>;
    *devices = AudioDevice::Outputs();
    for (std::vector<AudioDevice>::iterator it = devices->begin(); it!=devices->end(); it++) printf("%s\n", it->name().c_str());
    
    active = false;
    hidden = true;
    statusBMenu = new StatusBarMenu(@selector(toggleActive), @selector(toggleHidden), @selector(performQuit));
    
    audioBuffer = new AudioDataBuffer(AudioDevice::SystemDefault().streamFormat().mChannelsPerFrame, AudioDevice::SystemDefault().bufferSize()/AudioDevice::SystemDefault().streamFormat().mBytesPerFrame, AudioDevice::SystemDefault().streamFormat().mSampleRate);
    AudioDevice::Initialize(audioBuffer);
    
    AudioDevice::ExoDevice().start(); // initialize non-variable audio devices and synchronize them
    AudioDevice::SystemDefault().start();
    AudioDevice::Sync(2, &AudioDevice::SystemDefault(), &AudioDevice::ExoDevice());
    
    mainWindow = new WaveFormWindow(audioBuffer);
}

// cleanup all allocated memory
- (void) applicationWillTerminate:(NSNotification *)notification {
    free(devices);
    if (active) AudioDevice::SetOutput(AudioDevice::SystemDefault());
    mainWindow->hide();
    delete mainWindow;
    delete statusBMenu;
    delete audioBuffer;
}

// pause processes when application is deactivated
- (void) toggleActive {
    active = !active;
    AudioDevice::SetOutput(active?AudioDevice::ExoDevice():AudioDevice::SystemDefault());
    statusBMenu->toggleActive(active);
    if (!hidden) [self toggleHidden];
}

// show/hide windows when hidden
- (void) toggleHidden {
    if (hidden) mainWindow->show();
    else mainWindow->hide();
    hidden = !hidden;
}

// selector to trigger program quit
- (void) performQuit {
    [[NSApplication sharedApplication] terminate:self];
}

@end