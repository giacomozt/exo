#include "FFT.hpp"

// overload index operator to facilitate access to elements
template <typename T>
T& FFT::CircularArray<T>::operator[] (size_t index) {
    if (index<mSize) return mArray[index];
    else return (*this)[index-mSize];
}

// add an element to the circular array
template <typename T>
void FFT::CircularArray<T>::push(T value) {
    static size_t index = 0;
    mArray[index++] = value;
    if (index>=mSize) index = 0;
}

// swap the two pointers
template <typename T>
void FFT::swap(T** flip, T** flop) {
    T* temp = *flip;
    *flip = *flop;
    *flop = temp;
}

// reverse the bit representation of an integer
unsigned int FFT::bitReverse(unsigned int number, unsigned int numBits) {
    unsigned int reversed = 0;
    for (int h = 0; h<numBits; ++h) reversed |= ((number&(1<<h))>>h)<<(numBits-1-h);
        
    return reversed;
}

// calculate (or retrieve previously calculated) inverse root of unity 
std::complex<float> FFT::invRootOfUnity(unsigned int k, unsigned int N) {
    static std::unordered_map<unsigned int, std::complex<float>> stored({{0, std::complex<float>(1,0)}, {1, exp(std::complex<float>(0, -2*PI/N))}});
    if (k>N) return invRootOfUnity(k-N, N);
    if (stored.find(k)==stored.end()) stored[k] = stored[1]*invRootOfUnity(k-1, N);
    
    return stored[k];
}

// main fft algorithm
void FFT::analyze(float pos, bool calcBass) {
    // copy audio samples, scramble according to bit reversed radix map to "decimate in time"
    for (unsigned int h = 0; h<mNumSamps; ++h) {
        if (mRadixMap[h]<mNumSamps/2) (*flip)[h] = std::complex<float>((*mBuffer)[pos+mRadixMap[h]], 0);
        else (*flip)[h] = std::complex<float>(0, 0);
    }
    
    // begin recursive calculation and store results
    std::complex<float>* result = analyzeRecurse(flip, flop, 1);
    for (unsigned int h = 0; h<size(); ++h) mOut[h] = abs(result[h]); // maybe only calculate some?
    
    // analyze frequency spectrum
    if (calcBass) {
        float bassSum = 0.0;
        for (unsigned int h = 0; h<BASS_BIN_SIZE; ++h) bassSum += (BASS_BIN_SIZE-h)*mOut[h];
        mBassHistory->push(bassSum);
        float avg = 0.0, dev = 0.0;
        for (unsigned int h = 0; h<BASS_HISTORY_SIZE; ++h) {
            avg += (*mBassHistory)[h];
            dev += (*mBassHistory)[h]*(*mBassHistory)[h];
        }
        avg /= static_cast<float>(BASS_HISTORY_SIZE);
        dev = sqrt((dev-BASS_HISTORY_SIZE*avg*avg)/(BASS_HISTORY_SIZE-1.0));
        mBassBeat = bassSum>(avg+BASS_Z_MULT*dev);
        
        // print out 5000 samples of analyzed spectrum
        static int count = 0;
        if (!count++) {
            bass.open("bass_energy.txt");
            bassAvg.open("bass_avg.txt");
            bassBound.open("bass_bound.txt");
        }
        bass<<bassSum<<std::endl;
        bassAvg<<avg<<std::endl;
        bassBound<<dev<<std::endl;
        if (count==5000) {
            bass.close();
            bassAvg.close();
            bassBound.close();
        }
        
    }
}

// fft helper recursive function
std::complex<float>* FFT::analyzeRecurse(std::complex<float>** read, std::complex<float>** write, unsigned int depth) {
    unsigned int mult = mNumSamps/(1<<depth);
    unsigned int add = 1<<(depth-1);
    bool even = true;
    int count = 0;
    
    for (unsigned int h = 0; h<mNumSamps; ++h) {
        if (even) (*write)[h] = (*read)[h]+invRootOfUnity(h*mult, mNumSamps)*(*read)[h+add];
        else (*write)[h] = (*read)[h-add]+invRootOfUnity(h*mult, mNumSamps)*(*read)[h];
        
        if (++count==add) {
            count = 0;
            even = !even;
        }
    }
    
    swap(read, write);
    if (depth==mDegree) return *read;
    else return analyzeRecurse(read, write, depth+1);
}