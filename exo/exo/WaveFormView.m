#include <unistd.h>
#include <termios.h>
#include <fstream>
#include <ctime>
#include "WaveFormView.h"

@implementation WaveFormView

- (id)initWithFrame:(NSRect)frameRect buffer:(AudioDataBuffer*)_buffer {
    [super initWithFrame:frameRect];
    buffer = _buffer;
    float profile[FREQ_BIN];
    for (int h = 0; h<FREQ_BIN; ++h) {
        if (h<BASS_RANGE) profile[h] = exp(-h*h*h/1000.0);
        else profile[h] = 0;
    }
    fft = new FFT(_buffer, _buffer->frameCount(), 1000.0/_buffer->sampleRate()); // maybe?
    srand(static_cast<unsigned int>(time(NULL)));
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    [[NSColor colorWithRed:1 green:1 blue:1 alpha:0] setFill]; //
    NSRectFill(dirtyRect);
    
    fft->analyze(readPos, true); //** change to depending on if lightController is connected
    if (fft->bassBeat()) [[NSColor colorWithRed:static_cast<float>(rand())/RAND_MAX green:static_cast<float>(rand())/RAND_MAX blue:static_cast<float>(rand())/RAND_MAX alpha:1] setFill];
    else [[NSColor colorWithRed:0 green:0 blue:0 alpha:1] setFill];
    size_t size = fft->size();//*.5; // maybe?
    for (unsigned int h = 0; h<size; ++h) {
        NSRectFill(NSMakeRect(h*_frame.size.width/size, 0, _frame.size.width/size, 10*fft->amplitude(h)));
    }
    
    // Circular waveform display
    float radius = _frame.size.height/4.0;
    static float addRadius;
    if (fft->bassBeat()) addRadius = .9;
    else addRadius *= .75;
    float xCenter = _frame.size.width-radius;
    float yCenter = _frame.size.height-radius;
    NSBezierPath* path = [NSBezierPath bezierPath];
    [path moveToPoint:NSMakePoint(xCenter+(addRadius+.1)*radius, yCenter)];
    for (unsigned int h = 1; h<=512; ++h) [path lineToPoint:NSMakePoint(xCenter+(addRadius+.1)*radius*cos(TWO_PI*h/512.0), yCenter+(addRadius+.1)*radius*sin(TWO_PI*h/512.0))];
    [[NSColor colorWithRed:0 green:0 blue:0 alpha:.5] setFill];
    [path fill];
    
    /*for (unsigned int h = 0; h<buffer->frameCount(); ++h) {
        if (h) [path lineToPoint:NSMakePoint(xCenter+radius*(1+.5*(*buffer)[readPos+h])*cos(TWO_PI*h/buffer->frameCount()), yCenter+radius*(1+.5*(*buffer)[readPos+h])*sin(TWO_PI*h/buffer->frameCount()))];
        else [path moveToPoint:NSMakePoint(xCenter+radius*(1+.5*(*buffer)[readPos+h])*cos(TWO_PI*h/buffer->frameCount()), yCenter+radius*(1+.5*(*buffer)[readPos+h])*sin(TWO_PI*h/buffer->frameCount()))];
    }*/
}

- (void)draw:(Float64)time {
    readPos = time;
    [self setNeedsDisplay:true];
}

- (void)dealloc {
    if (controller>=0) {
        //tcsetattr(controller, TCSANOW, &oldS);
        close(controller);
    }
    delete fft;
    [super dealloc];
}

- (void)initController {
    controller = open("/dev/cu.usbmodem1421", O_RDWR);
    if (controller<0) printf("could not connect to light controller\n");
    else usleep(5000000);
    int msg = 1;
    write(controller, &msg, sizeof(msg));
}

@end
