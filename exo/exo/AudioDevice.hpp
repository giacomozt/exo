#ifndef AUDIODEVICE_HPP
#define AUDIODEVICE_HPP

#include <cstdarg>
#include <vector>
#include <string>
#include <CoreAudio/CoreAudio.h>
#include <AudioUnit/AudioUnit.h>
#include "AudioDataBuffer.hpp"

// class for interfacing with audio devices (speakers and virtual exo device)
class AudioDevice {
protected:
    static AudioDevice systemDefault;
    static AudioDevice exoDevice;
    static AudioDataBuffer* buffer;
    
    bool on = false;
    bool output = true;
    bool sync = false;
    AudioDeviceID id = -1;
    AudioDeviceIOProcID procID = NULL;
    Float64 time = 0.0;
    Float64 lastRead;
    
    static bool isOutput(AudioDeviceID ofDeviceID);
    
    static AudioDeviceID IDByName(const char* target);
    
public:
    static void Initialize(AudioDataBuffer* _buffer) {
        buffer = _buffer;
    }
    
    // synchronize audio devices to reset playhead positioning
    static void Sync(size_t count, ...) {
        va_list args;
        va_start(args, count);
        for (int h = 0; h<count; ++h) va_arg(args, AudioDevice*)->sync = true;
    }
    
    // return a list of available devices
    static std::vector<AudioDevice> Outputs() {
        AudioObjectPropertyAddress listAddress = {kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
        AudioObjectPropertyAddress nameAddress = {kAudioDevicePropertyDeviceName, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
        std::vector<AudioDevice> outputs;
        
        int numDevices;
        UInt32 size;
        
        if (AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &listAddress, 0, NULL, &size)!=noErr) return outputs;
        numDevices = size/sizeof(AudioDeviceID);
        AudioDeviceID* deviceID;
        if (AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &listAddress, 0, NULL, &size)!=noErr) return outputs;
        deviceID = (AudioDeviceID*)malloc(sizeof(AudioDeviceID)*numDevices);
        if (AudioObjectGetPropertyData(kAudioObjectSystemObject, &listAddress, 0, NULL, &size, deviceID)!=noErr) {
            free(deviceID);
            return outputs;
        }
        
        for (int h = 0; h<numDevices; ++h) printf("%d\n", deviceID[h]);
        
        for (int h = 0; h<numDevices; ++h) {
            char buffer[64];
            size = sizeof(buffer);
            AudioObjectGetPropertyData(deviceID[h], &nameAddress, 0, NULL, &size, buffer);
            if (isOutput(deviceID[h])&&strcmp(buffer, "exo")&&strcmp(buffer, "Built-in Output")) outputs.push_back(AudioDevice(buffer));
        }
        free(deviceID);
        
        return outputs;
    }
    
    // check if an AudioDevice instance was correctly initialized
    static bool Failed(const AudioDevice device) {
        return device.id==-1;
    }
    
    // check if two AudioDevice instances are compatible (same stream format, channel count etc)
    static bool Compatible(const AudioDevice& device1, const AudioDevice& device2) {
        if (Failed(device1)||Failed(device2)) return false;
        if (device1.bufferSize()!=device2.bufferSize()) return false;
        if (device1.streamFormat().mSampleRate!=device2.streamFormat().mSampleRate) return false;
        if (device1.streamFormat().mFormatFlags!=device2.streamFormat().mFormatFlags) return false;
        if (device1.streamFormat().mBytesPerPacket!=device2.streamFormat().mBytesPerPacket) return false;
        if (device1.streamFormat().mFramesPerPacket!=device2.streamFormat().mFramesPerPacket) return false;
        if (device1.streamFormat().mChannelsPerFrame!=device2.streamFormat().mChannelsPerFrame) return false;
        if (device1.streamFormat().mBytesPerFrame!=device2.streamFormat().mBytesPerFrame) return false;
        if (device1.streamFormat().mBitsPerChannel!=device2.streamFormat().mBitsPerChannel) return false;
        return true;
    }
    
    static void SetOutput(AudioDevice device);
    
    static AudioDevice& SystemDefault() {
        return systemDefault;
    }
    
    static AudioDevice& ExoDevice() {
        return exoDevice;
    }
    
    static OSStatus readProc(AudioDeviceID inDevice, const AudioTimeStamp* inNow, const AudioBufferList* inInputData, const AudioTimeStamp* inInputTime, AudioBufferList* outOutputData, const AudioTimeStamp* inOutputTime, void* inClientData);
    
    static OSStatus writeProc(AudioDeviceID inDevice, const AudioTimeStamp* inNow, const AudioBufferList* inInputData, const AudioTimeStamp* inInputTime,
                              AudioBufferList* outOutputData, const AudioTimeStamp* inOutputTime, void* inClientData);
    
    AudioDevice(const char* target) {
        id = IDByName(target);
        if (!strcmp(target, "exo")) output = false;
    }
    
    void start() {
        if (!procID) AudioDeviceCreateIOProcID(id, (output?readProc:writeProc), this, &procID);
        AudioDeviceStart(id, procID);
    }
    
    void pause() {
        AudioDeviceStop(id, procID);
    }
    
    Float64 offset() {
        return time;
    }
    
    Float64 last() {
        return lastRead;
    }
    
    AudioDeviceID& _id() {
        return id;
    }
    
    AudioDeviceIOProcID& _procID() {
        return procID;
    }
    
    UInt32 bufferSize() const {
        AudioObjectPropertyAddress bufferSizeAddress = {kAudioDevicePropertyBufferSize, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
        UInt32 res;
        UInt32 size = sizeof(res);
        AudioObjectGetPropertyData(id, &bufferSizeAddress, 0, NULL, &size, &res);
        return res;
    }
    
    AudioStreamBasicDescription streamFormat() const {
        AudioObjectPropertyAddress bufferSizeAddress = {kAudioDevicePropertyStreamFormat, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
        AudioStreamBasicDescription res;
        UInt32 size = sizeof(res);
        AudioObjectGetPropertyData(id, &bufferSizeAddress, 0, NULL, &size, &res);
        return res;
    }
    
    std::string name() {
        AudioObjectPropertyAddress nameAddress = {kAudioDevicePropertyDeviceName, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
        char buffer[64];
        UInt32 size = sizeof(buffer);
        AudioObjectGetPropertyData(id, &nameAddress, 0, NULL, &size, buffer);
        return std::string(buffer);
    }
};

#endif
