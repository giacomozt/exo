#ifndef DFT_HPP
#define DFT_HPP

#include <complex>
#include <cmath>
#include "AudioDataBuffer.hpp"

#define TWO_PI 6.28318530718
#define FREQ_BIN 128
#define BASS_RANGE 20
#define SHORT_AVG_SAMPS 1000

class DFT {
protected:
    bool bassBeatOn;
    unsigned int shortSamps;
    unsigned int longSamps;
    unsigned int maxIndex;
    float currAvg;
    float shortAvg;
    float longAvg;
    float lastAvg;
    float* scale;
    float* bassProfile;
    float* result;
    AudioDataBuffer* buffer;
    
    void calcAvgs() {
        currAvg = 0.0;
        for (int h = 0; h<BASS_RANGE; ++h) currAvg += bassProfile[h]*result[h];
        ++shortSamps;
        ++longSamps;
        shortAvg = (shortAvg*(shortSamps-1)+currAvg)/shortSamps;
        longAvg = (longAvg*(longSamps-1)+currAvg)/longSamps;
        if (shortSamps==SHORT_AVG_SAMPS) {
            if (std::abs(shortAvg-longAvg)/longAvg>.2) {
                longAvg = 0.0;
                longSamps = 0;
            }
            shortAvg = 0.0;
            shortSamps = 0;
        }
    }
    
public:
    DFT(float* _bassProfile, AudioDataBuffer* _buffer) {
        shortSamps = 0;
        longSamps = 0;
        longAvg = 0.0;
        shortAvg = 0.0;
        
        scale = new float[FREQ_BIN];
        //for (int h = 0; h<FREQ_BIN; ++h) scale[h] = -pow(h, 2)*TWO_PI/(2*pow(FREQ_BIN, 2)*3); //?
        //for (int h = 0; h<FREQ_BIN; ++h) scale[h] = -pow(h, 1.1)*TWO_PI/(2*pow(FREQ_BIN, 1.1)*4); //?
        for (int h = 0; h<FREQ_BIN; ++h) scale[h] = -static_cast<double>(h)*TWO_PI/(2*FREQ_BIN);
        bassProfile = new float[FREQ_BIN];
        for (int h = 0; h<FREQ_BIN; ++h) bassProfile[h] = _bassProfile[h];
        result = new float[FREQ_BIN];
        
        buffer = _buffer;
    }
    
    ~DFT() {
        delete[] scale;
        delete[] bassProfile;
        delete[] result;
    }
    
    int count() {
        return FREQ_BIN;
    }
    
    void analyze(float pos) {
        int newMax = 0;
        for (int h = 0; h<FREQ_BIN; ++h) {
            float amp = 0.0, phase = 0.0;
            int first = 0;
            int last = buffer->frameCount();
            
            for (int i = first; i<last; ++i) {
                std::complex<float> c1((*buffer)[static_cast<size_t>(pos+i)], 0.0);
                //std::complex<float> c1(buffer->readFrame(pos+i)[0], 0);
                std::complex<float> c2(0, scale[h]*i); //*4?
                std::complex<float> res(c1*exp(c2));
                amp += real(res);
                phase += imag(res);
            }
            result[h] = pow(amp,2)+pow(phase,2);
            
            if (result[h]>result[newMax]) newMax = h;
        }
        
        calcAvgs();
    }
    
    bool bassBeat() {
        if (currAvg>1.1*longAvg) return true;
        else return false;
    }
    
    float amplitude(int index) {
        return result[index];
    }
};

#endif
