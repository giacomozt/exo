#include "AudioDevice.hpp"

// declare non-variable devices (devices that will be present every runtime)
AudioDevice AudioDevice::systemDefault("Built-in Output");
AudioDevice AudioDevice::exoDevice("exo");
AudioDataBuffer* AudioDevice::buffer = NULL;

// check if input device is an output or input
bool AudioDevice::isOutput(AudioDeviceID ofDeviceID) {
    AudioObjectPropertyAddress streamAddress = {kAudioDevicePropertyStreams, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    AudioStreamID streamID[2] = {0};
    UInt32 size = sizeof(streamID);
    AudioObjectGetPropertyData(ofDeviceID, &streamAddress, 0, NULL, &size, &streamID);
    AudioObjectPropertyAddress directionAddress = {kAudioStreamPropertyDirection, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    for (int h = 0; h<2; h++) {
        int res = -1;
        UInt32 rSize = sizeof(res);
        AudioObjectGetPropertyData(streamID[h], &directionAddress, 0, NULL, &rSize, &res);
        if (!streamID[h]) return false;
        if (res==0) return true;
    }
    return false;
}

// finds the AudioDeviceID of the given device name (if it exists, returns -1 if not found)
AudioDeviceID AudioDevice::IDByName(const char* target) {
    int numDevices;
    UInt32 size;
    AudioDeviceID targetDeviceID = -1;
    AudioDeviceID* deviceID = NULL;
    AudioObjectPropertyAddress listAddress = {kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    AudioObjectPropertyAddress nameAddress = {kAudioDevicePropertyDeviceName, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    if (AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &listAddress, 0, NULL, &size)!=noErr) return -1;
    numDevices = size/sizeof(AudioDeviceID);
    deviceID = (AudioDeviceID*)malloc(sizeof(AudioDeviceID)*numDevices);
    if (AudioObjectGetPropertyData(kAudioObjectSystemObject, &listAddress, 0, NULL, &size, deviceID)!=noErr) return -1;
    for (int h = 0; h<numDevices; h++) {
        char buffer[64];
        size = sizeof(buffer);
        AudioObjectGetPropertyData(deviceID[h], &nameAddress, 0, NULL, &size, buffer);
        if (!strcmp(buffer, target)) {
            targetDeviceID = deviceID[h];
            break;
        }
    }
    
    free(deviceID);
    return targetDeviceID;
}

// set the output device of the system
void AudioDevice::SetOutput(AudioDevice device) {
    AudioDeviceID deviceID = device.id;
    AudioObjectPropertyAddress defaultAddress = {kAudioHardwarePropertyDefaultOutputDevice, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    UInt32 deviceSize = sizeof(deviceID);
    AudioObjectSetPropertyData(kAudioObjectSystemObject, &defaultAddress, 0, NULL, deviceSize, &deviceID);
}

// callback function to read data from the audio buffer
OSStatus AudioDevice::readProc(AudioDeviceID inDevice, const AudioTimeStamp* inNow, const AudioBufferList* inInputData, const AudioTimeStamp* inInputTime, AudioBufferList* outOutputData, const AudioTimeStamp* inOutputTime, void* inClientData) {
    AudioDevice* device = (AudioDevice*)inClientData;
    int numFrames = device->bufferSize()/device->streamFormat().mBytesPerFrame;
    int numChans = device->streamFormat().mChannelsPerFrame;
    float* speakers = (float*)outOutputData->mBuffers[0].mData;
    
    if (device->sync) {
        device->time = inOutputTime->mSampleTime;
        device->sync = false;
    }
    
    for (int h = 0; h<numFrames; h++) {
        float* read = buffer->readFrame(inOutputTime->mSampleTime-device->time+1.0*h);
        for (int i = 0; i<numChans; i++) {
            *speakers++ = read[i];
        }
    }
    device->lastRead = inOutputTime->mSampleTime-device->time;
    
    return kAudioHardwareNoError;
}

// callback function to write data to the audio buffer
OSStatus AudioDevice::writeProc(AudioDeviceID inDevice, const AudioTimeStamp* inNow, const AudioBufferList* inInputData, const AudioTimeStamp* inInputTime,
                          AudioBufferList* outOutputData, const AudioTimeStamp* inOutputTime, void* inClientData) {
    AudioDevice* device = (AudioDevice*)inClientData;
    int numFrames = device->bufferSize()/device->streamFormat().mBytesPerFrame;
    int numChans = device->streamFormat().mChannelsPerFrame;
    float* mic = (float*)inInputData->mBuffers[0].mData;
    
    for (int h = 0; h<numFrames; h++) {
        float write[numChans];
        for (int i = 0; i<numChans; i++) {
            write[i] = *mic++;
        }
        buffer->writeFrame(write);
    }
    
    return kAudioHardwareNoError;
}