#ifndef AUDIODATABUFFER_HPP
#define AUDIODATABUFFER_HPP

#include <cstdio>
#include <cstdlib>

#define BUFF_MULT 32 // factor of extra frames to store in buffer to prevent live overwrite
#define DEF_CHANNEL 0 // number of the default channel to analyze

// class for storing/interacting with live audio data
class AudioDataBuffer {
protected:
    int mNumChans;
    int mNumFrames;
    int mBufferhead;
    float mFirstTimestamp;
    float mSampleRate;
    float** data;
    
public:
    AudioDataBuffer(int numChans, int numFrames, float sampleRate) {
        mNumChans = numChans;
        mNumFrames = BUFF_MULT*numFrames;
        mBufferhead = 0;
        mSampleRate = sampleRate;
        
        data = (float**)malloc(sizeof(float*)*mNumFrames);
        for (int h = 0; h<mNumFrames; h++) data[h] = (float*)malloc(sizeof(float)*mNumChans);
    }
    
    ~AudioDataBuffer() {
        for (int h = 0; h<mNumChans; h++) free(data[h]);
        free(data);
    }
    
    float operator[] (size_t index) {
        index = index%mNumFrames;
        return data[index][DEF_CHANNEL];
    }
    
    void writeFrame(float* inData) {
        for (int h = 0; h<mNumChans; ++h) {
            (data[mBufferhead])[h] = inData[h];
        }
        mBufferhead = (mBufferhead+1)%mNumFrames;
    }
    
    int frameCount() {
        return mNumFrames/BUFF_MULT;
    }
    
    float sampleRate() {
        return mSampleRate;
    }
    
    float* readFrame(float timestamp) {
        int index = abs(static_cast<int>(timestamp)%mNumFrames);
        return data[index];
    }
};

#endif
