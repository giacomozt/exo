#ifndef APPDELEGATE_H
#define APPDELEGATE_H

#include "StatusBarMenu.hpp"
#include "AudioDevice.hpp"
#include "WaveFormWindow.hpp"

#define FRAMECOUNT 16
#define ANIMCOUNT 50

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    bool active;
    bool hidden;
    std::vector<AudioDevice>* devices;
    StatusBarMenu* statusBMenu;
    AudioDataBuffer* audioBuffer;
    WaveFormWindow* mainWindow;
}
@end

#endif