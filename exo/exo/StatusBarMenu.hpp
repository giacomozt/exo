#ifndef MENUBARMENU_HPP
#define MENUBARMENU_HPP

#include <stdio.h>
#include <Cocoa/Cocoa.h>
#include <CoreFoundation/CoreFoundation.h>

#define FRAMECOUNT 16
#define ANIMCOUNT 50

// class for interfacing with the NSMenuBar display
class StatusBarMenu {
protected:
    id delegate;
    SEL triggerQuit;
    bool activeAnimation = false;
    unsigned short cFrame = 0;
    unsigned short getFrame[ANIMCOUNT];
    CFRunLoopTimerRef animTimer;
    NSImage* frame[FRAMECOUNT];
    NSStatusItem* menuBar;
    NSMenu* menu;
    
    // callback to increment the animation
    static void staticIncrementAnimation(CFRunLoopTimerRef timer, void* info) {
        ((StatusBarMenu*)info)->incrementAnimation();
    }
    
    void startAnimation() {
        CFRunLoopTimerContext context = {0, this, 0, 0, 0};
        animTimer = CFRunLoopTimerCreate(kCFAllocatorDefault, CFAbsoluteTimeGetCurrent(), 2.0/ANIMCOUNT, 0, 0, staticIncrementAnimation, &context);
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), animTimer, kCFRunLoopCommonModes);
    }
    
    void stopAnimation() {
        CFRunLoopTimerInvalidate(animTimer);
    }
    
    void incrementAnimation() {
        cFrame = (cFrame+1)%ANIMCOUNT;
        [menuBar setImage:frame[getFrame[cFrame]]];
    }
    
    void clearMenu(NSMenu* menu) {
        while ([menu numberOfItems]>0) {
            if (![[menu itemAtIndex:0] isSeparatorItem]) {
                if ([[menu itemAtIndex:0] hasSubmenu]) {
                    clearMenu([[menu itemAtIndex:0] submenu]);
                    [[menu itemAtIndex:0] setSubmenu:NULL];
                } else {
                    [[menu itemAtIndex:0] release];
                }
            }
            [menu removeItemAtIndex:0];
        }
        [menu release];
    }
    
public:
    StatusBarMenu(SEL _toggleActive, SEL _toggleHidden, SEL _triggerQuit) {
        for (int h = 0; h<FRAMECOUNT; h++) getFrame[h] = h;
        for (int h = 1; h<=FRAMECOUNT; h++) getFrame[h+FRAMECOUNT-2] = 0;
        for (int h = 2*FRAMECOUNT-2; h<ANIMCOUNT; h++) getFrame[h] = 0;
        for (int h = 0; h<FRAMECOUNT; h++) {
            frame[h] = [NSImage imageNamed:[NSString stringWithFormat:@"icon%d.tiff", h]];
            [frame[h] setTemplate:true];
        }
        
        // initialize static elements
        menuBar = [[[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength] retain];
        [menuBar setImage:frame[cFrame]];
        [menuBar setHighlightMode:true];
        [menuBar setMenu:[[NSMenu alloc] initWithTitle:@""]];
        [[menuBar menu] addItem:[[NSMenuItem alloc] initWithTitle:@"exo" action:NULL keyEquivalent:@""]];
        [[menuBar menu] addItem:[NSMenuItem separatorItem]];
        /*[[menuBar menu] addItem:[[NSMenuItem alloc] initWithTitle:@"outputs" action:NULL keyEquivalent:@""]];
        [[[menuBar menu] itemWithTitle:@"outputs"] setSubmenu:[[NSMenu alloc] initWithTitle:@""]];
        [[[[menuBar menu] itemWithTitle:@"outputs"] submenu] addItem:[[NSMenuItem alloc] initWithTitle:@"test" action:NULL keyEquivalent:@""]];
        [[[menuBar menu] itemWithTitle:@"outputs"] setHidden:true];*/
        [[menuBar menu] addItem:[[NSMenuItem alloc] initWithTitle:@"toggle HUD" action:_toggleHidden keyEquivalent:@""]];
        [[[menuBar menu] itemWithTitle:@"toggle HUD"] setHidden:true];
        [[menuBar menu] addItem:[[NSMenuItem alloc] initWithTitle:@"activate" action:_toggleActive keyEquivalent:@""]];
        [[menuBar menu] addItem:[[NSMenuItem alloc] initWithTitle:@"quit" action:_triggerQuit keyEquivalent:@""]];
        
        startAnimation();
        //[delegate performSelector:triggerQuit withObject:NULL afterDelay:0];
    }
    
    ~StatusBarMenu() {
        clearMenu([menuBar menu]);
    }
    
    void toggleActive(bool active) {
        for (int h = 1; h<=FRAMECOUNT; h++) getFrame[h+FRAMECOUNT-2] = active?(FRAMECOUNT-h):0;
        //[[[menuBar menu] itemWithTitle:@"outputs"] setHidden:!active];
        [[[menuBar menu] itemWithTitle:(active?@"activate":@"deactivate")] setTitle:(active?@"deactivate":@"activate")];
        [[[menuBar menu] itemWithTitle:@"toggle HUD"] setHidden:!active];
    }
    
};

#endif
