#ifndef WAVEFORMWINDOW_HPP
#define WAVEFORMWINDOW_HPP

#include <Cocoa/Cocoa.h>
#include <ApplicationServices/ApplicationServices.h>
#include <CoreFoundation/CoreFoundation.h>
#include <CoreGraphics/CoreGraphics.h>
#include "AudioDataBuffer.hpp"
#include "AudioDevice.hpp"
#include "WaveFormView.h"

// class for encapsulating NSWindow class
class WaveFormWindow {
protected:
    CVDisplayLinkRef link;
    NSWindow* window;
    WaveFormView* view;
    AudioDevice* device = NULL;
    
public:
    // callback to trigger redrawing of window with updated info
    static CVReturn drawCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *inNow, const CVTimeStamp *inOutputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void *displayLinkContext) {
        if (!(((WaveFormWindow*)displayLinkContext)->device)) [((WaveFormWindow*)displayLinkContext)->view draw:AudioDevice::SystemDefault().last()];
        else [((WaveFormWindow*)displayLinkContext)->view draw:((WaveFormWindow*)displayLinkContext)->device->last()];
        return kCVReturnSuccess;
    }
    
    WaveFormWindow(AudioDataBuffer* _buffer) {
        CVDisplayLinkCreateWithActiveCGDisplays(&link);
        CVDisplayLinkSetOutputCallback(link, drawCallback, this);
        CVDisplayLinkStart(link);
        
        window = [[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, [[NSScreen mainScreen] frame].size.width, [[NSScreen mainScreen] frame].size.height) styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:false];
        view = [[WaveFormView alloc] initWithFrame:NSMakeRect(0, 0, window.frame.size.width, window.frame.size.height) buffer:_buffer];
        [window setContentView:view];
        [window setLevel:NSPopUpMenuWindowLevel];
        [window setMovableByWindowBackground:false];
        
        [view initController];
    }
    
    ~WaveFormWindow() {
        [view release];
        [window release];
    }
    
    void setDevice(AudioDevice* _device) {
        device = _device;
    }
    
    void clearDevice() {
        device = NULL;
    }
    
    void show() {
        CVDisplayLinkStart(link);
        [window makeKeyAndOrderFront:NULL];
    }
    
    void hide() {
        [window orderOut:NULL];
        //CVDisplayLinkStop(link); // leave on for arduino light control
    }
};

#endif